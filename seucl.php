<?php
/**
 * Main module Simple EU Cookie Law
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

class Seucl extends Module
{
    public function __construct()
    {
        $this->name = 'seucl';
        $this->tab = 'front_office_features';
        $this->version = '1.01';
        $this->author = 'MAGES Michał Jendraszczyk';

        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Simple EU Cookie Law');

        $this->description = $this->l('Enter yours policy cookie information');
 
        $this->ConfirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->module_key = 'ddd6142b63cb185868d4c97060155b66';
    }

    // instalacja

    public function install()
    {
  
        Configuration::updateValue('seucl__kolor', '#f4f4f4');
        //Configuration::updateValue("seucl__tresc", "This page uses cookies. Learn more about the purpose and use of cookies in your browser. By using the website, use cookies, according to the current settings");

        Configuration::updateValue('seucl__pozycja', 'bottom');
        Configuration::updateValue('seucl__kolor_font', '#343434');

        Configuration::updateValue('seucl__val_btn', 'Accept');

        Configuration::updateValue('seucl__cookies_expires', '1');

        Configuration::updateValue('seucl__kolor_font_btn', '#ffffff');
        Configuration::updateValue('seucl__kolor_bg_btn', '#343434');

        Configuration::updateValue('seucl__img', 'views/img/4790d881c75a3f63bafe4a8abe883123.png');

        Configuration::updateValue('seucl__policy_url', '#');
        Configuration::updateValue('seucl__policy_name', ''); // Remember me
        
        return parent::install() && $this->registerHook('displayTop') && $this->registerHook('displayHeader');
    }

    //uzyskanie contentu
    public function postProcess()
    {
        if (Tools::isSubmit('submitAddconfiguration')) {
            $this->_clearCache('views/templates/hook/seucl.tpl');

            if (isset($_FILES['seucl__img']) && isset($_FILES['seucl__img']['tmp_name']) && !empty($_FILES['seucl__img']['tmp_name'])) {
                if ($error = ImageManager::validateUpload($_FILES['seucl__img'], 2000000)) {
                    return $this->displayError($this->l('Invalid file upload. Max size is 2MB and must have extension jpg, jpeg, png, gif.')).'';
                } else {
                    $ext = Tools::substr($_FILES['seucl__img']['name'], strrpos($_FILES['seucl__img']['name'], '.') + 1);
                    $file_name = md5($_FILES['seucl__img']['name']).'.'.$ext;

                    if (!move_uploaded_file($_FILES['seucl__img']['tmp_name'], dirname(__FILE__).'/'.$file_name)) {
                        return $this->displayError($this->l('An error occurred while attempting to upload the file.'));
                    } else {
                        if (Configuration::get('seucl__img') != 'img/4790d881c75a3f63bafe4a8abe883123.png') {
                            @unlink(dirname(__FILE__).'/'.Configuration::get('seucl__img'));
                        }
                        Configuration::updateValue('seucl__img', $file_name);
                        $this->_clearCache('views/templates/hook/seucl.tpl');

                        return $this->displayConfirmation($this->l('Uploaded successfully')); // get info
                    }
                }
            }
            if (!empty(Tools::getValue('seucl__cookies_expires'))) {
                if (Validate::isUnsignedInt(Tools::getValue('seucl__cookies_expires')) == true) {
                    Configuration::updateValue('seucl__cookies_expires', Tools::getValue('seucl__cookies_expires'));

                    return $this->displayConfirmation($this->l('Save successfully'));
                } else {
                    return $this->displayError($this->l('Cookies expired value must be integer number'));
                }
            }
            Configuration::updateValue('seucl__kolor', Tools::getValue('seucl__kolor'));

            $languages = Language::getLanguages(false);
            foreach ($languages as $lang) {
                Configuration::updateValue('seucl__tresc_'.$lang['id_lang'], Tools::getValue('seucl__tresc_'.$lang['id_lang']));
            }
            Configuration::updateValue('seucl__pozycja', Tools::getValue('seucl__pozycja'));
            Configuration::updateValue('seucl__kolor_font', Tools::getValue('seucl__kolor_font'));
            Configuration::updateValue('seucl__val_btn', Tools::getValue('seucl__val_btn'));

            Configuration::updateValue('seucl__kolor_font_btn', Tools::getValue('seucl__kolor_font_btn'));
            Configuration::updateValue('seucl__kolor_bg_btn', Tools::getValue('seucl__kolor_bg_btn'));

            Configuration::updateValue('seucl__policy_url', Tools::getValue('seucl__policy_url'));
            Configuration::updateValue('seucl__policy_name', Tools::getValue('seucl__policy_name'));

            return $this->displayConfirmation($this->l('Save changes'));
            $this->hookDisplayTop();
            $this->hookDisplayBanner();
        }

        return '';
    }

    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }

    public function renderForm()
    {
        $default_lang = (int) Configuration::get('PS_LANG_DEFAULT');
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(array(
                        'type' => 'textarea',
                        'label' => $this->l('Cookies info'),
                        'name' => 'seucl__tresc',
                        'required' => false,
                        'lang' => true,
                        'desc' => $this->l('Write yours cookies info'),
                    ),
                    array(
                        'type' => 'file',
                        'name' => 'seucl__img',
                        'label' => $this->l('Cookies image'),
                        'desc' => $this->l('Set custom image on the cookies bar. Allow only files .jpg, .png, .gif, .jpeg'),
                        'thumb' => '../modules/'.$this->name.'/'.Configuration::get('seucl__img'),
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Bar background color'),
                        'required' => false,
                        'data-hex' => true,
                        'desc' => $this->l('Set background color the cookies bar'),
                        'name' => 'seucl__kolor',
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Bar font color'),
                        'required' => false,
                        'data-hex' => true,
                        'desc' => $this->l('Set font color the cookies bar'),
                        'name' => 'seucl__kolor_font',
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Bar position'),
                        'name' => 'seucl__pozycja',
                        'required' => false,
                        'lang' => false,
                        'desc' => $this->l('Cookies bar position'),
                        'options' => array(
                        'query' => $pozycje = array(
                        array(
                            'seucl__id_pozycja' => 'top',
                            'seucl__nazwa_pozycja' => 'top',
                        ),
                        array(
                            'seucl__id_pozycja' => 'bottom',
                            'seucl__nazwa_pozycja' => 'bottom',
                        ),
                            ),
                            'id' => 'seucl__id_pozycja',
                            'name' => 'seucl__nazwa_pozycja',
                        ),
                    ),
                    
                    array(
                        'name' => 'seucl__cookies_expires',
                        'type' => 'text',
                        'label' => 'Cookies expired [of days]',
                        'desc' => 'Set how long cookies will be stored'
                    ),
                    array(
                        'name' => 'seucl__policy_url',
                        'type' => 'text',
                        'label' => 'Privacy policy URL',
                        'desc' => 'Set url link to your privacy policy',
                    ),
                    array(
                        'name' => 'seucl__policy_name',
                        'type' => 'text',
                        'label' => 'Privacy policy name',
                        'desc' => 'Set name to link to your privacy policy',
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Button background color'),
                        'required' => false,
                        'data-hex' => true,
                        'desc' => $this->l('Set background color on button the cookies bar'),
                        'name' => 'seucl__kolor_bg_btn',
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Button font color'),
                        'required' => false,
                        'data-hex' => true,
                        'desc' => $this->l('Set font color on button the cookies bar'),
                        'name' => 'seucl__kolor_font_btn',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Button value'),
                        'required' => false,
                        'data-hex' => true,
                        'desc' => $this->l('Set button value on the cookies bar'),
                        'name' => 'seucl__val_btn',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );

        $helper = new HelperForm();

        $helper->table = $this->table;
        $helper->module = $this;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;

        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;

        $this->fields_form = array();
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->submit_action = 'submitAddconfiguration';
        $helper->tpl_vars = array(
            'fields_value' => array(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        $languages = Language::getLanguages(false);

        foreach ($languages as $k => $lang) {
            $helper->tpl_vars['fields_value']['seucl__tresc'][$lang['id_lang']] = Tools::getValue('seucl__tresc_'.$lang['id_lang'], Configuration::get('seucl__tresc_'.$lang['id_lang']));
        }

        $helper->tpl_vars['fields_value']['seucl__kolor'] = Tools::getValue('seucl__kolor', Configuration::get('seucl__kolor'));
        $helper->tpl_vars['fields_value']['seucl__kolor_font'] = Tools::getValue('seucl__kolor_font', Configuration::get('seucl__kolor_font'));

        $helper->tpl_vars['fields_value']['seucl__pozycja'] = Tools::getValue('seucl__pozycja', Configuration::get('seucl__pozycja'));

        $helper->tpl_vars['fields_value']['seucl__cookies_expires'] = Tools::getValue('seucl__cookies_expires', Configuration::get('seucl__cookies_expires'));

        $helper->tpl_vars['fields_value']['seucl__img'] = Tools::getValue('seucl__img', Configuration::get('seucl__img'));

        $helper->tpl_vars['fields_value']['seucl__val_btn'] = Tools::getValue('seucl__val_btn', Configuration::get('seucl__val_btn'));

        $helper->tpl_vars['fields_value']['seucl__kolor_bg_btn'] = Tools::getValue('seucl__kolor_bg_btn', Configuration::get('seucl__kolor_bg_btn'));
        $helper->tpl_vars['fields_value']['seucl__kolor_font_btn'] = Tools::getValue('seucl__kolor_font_btn', Configuration::get('seucl__kolor_font_btn'));

        $helper->tpl_vars['fields_value']['seucl__policy_url'] = Tools::getValue('seucl__policy_url', Configuration::get('seucl__policy_url'));
        $helper->tpl_vars['fields_value']['seucl__policy_name'] = Tools::getValue('seucl__policy_name', Configuration::get('seucl__policy_name'));

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $data = array(
            'seucl__kolor' => Tools::getValue('seucl__kolor', Configuration::get('seucl__kolor')),
            'seucl__tresc' => Tools::getValue('seucl__tresc', Configuration::get('seucl__tresc')),
        );

        return $data;
    }

    public function setMedia()
    {
        parent::setMedia();
        
        $this->context->controller->registerStylesheet(
            'modules-seucl',
            'modules/'.$this->name.'/views/css/seucl.css',
            array('media' => 'all', 'priority' => 150)
        );
    }

    public function hookdisplayHeader($params)
    {
        $this->context->controller->registerStylesheet('modules-seucl', 'modules/'.$this->name.'/views/css/secul.css', array('media' => 'all', 'priority' => 150));
        $this->context->controller->registerJavascript('modules-seucl', 'modules/'.$this->name.'/views/js/secul.js', array('position' => 'bottom', 'priority' => 150));
    }
  
    public function hookTop()
    {
        $languages = Language::getLanguages(false);

        $seucl_tresc = array();
        foreach ($languages as $lang) {
            $seucl_tresc[$lang['id_lang']] = Configuration::get('seucl__tresc_'.$lang['id_lang']); // tu jako wart konkretna rzecz z tablicy
        }
        $this->smarty->assign('seucl', $seucl_tresc);

        $this->smarty->assign('pozycjacookie', Configuration::get('seucl__pozycja'));
        $this->smarty->assign('kolor', Configuration::get('seucl__kolor'));
        $this->smarty->assign('font_kolor', Configuration::get('seucl__kolor_font'));

        $this->smarty->assign('expires', Configuration::get('seucl__cookies_expires'));
        $this->smarty->assign('img_cookie', Configuration::get('seucl__img'));

        $this->smarty->assign('btn_wartosc', $this->l('Save', 'Admin.Global'));
        $this->smarty->assign('btn_bg', Configuration::get('seucl__kolor_bg_btn'));
        $this->smarty->assign('btn_font', Configuration::get('seucl__kolor_font_btn'));

        $this->smarty->assign('policy_url', Configuration::get('seucl__policy_url'));
        $this->smarty->assign('policy_name', Configuration::get('seucl__policy_name')); // $this->translator->trans("Remember me",'',"Admin.Global")

        $this->smarty->assign('is_https', (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] == 'on' ? 1 : 0));

        $this->smarty->assign('main_url', $this->context->link->getPageLink('/', true));

        return $this->display(dirname(__FILE__), 'views/templates/hook/seucl.tpl');
    }

    public function hookDisplayBanner()
    {
        $this->smarty->assign('seucl', Configuration::get('seucl__tresc'));
        $this->smarty->assign('pozycjacookie', Configuration::get('seucl__pozycja'));
        $this->smarty->assign('kolor', Configuration::get('seucl__kolor'));
        $this->smarty->assign('font_kolor', Configuration::get('seucl__kolor_font'));

        $this->smarty->assign('expires', Configuration::get('seucl__cookies_expires'));
        $this->smarty->assign('img_cookie', Configuration::get('seucl__img'));

        $this->smarty->assign('btn_wartosc', Configuration::get('seucl__val_btn'));
        $this->smarty->assign('btn_bg', Configuration::get('seucl__kolor_bg_btn'));
        $this->smarty->assign('btn_font', Configuration::get('seucl__kolor_font_btn'));

        $this->smarty->assign('main_url', Tools::getHttpHost(false));

        return $this->display(dirname(__FILE__), 'views/templates/hook/seucl.tpl');
    }

    // delete func

    public function uninstall()
    {
        return parent::uninstall();
    }
}
