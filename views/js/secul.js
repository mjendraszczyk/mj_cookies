/**
 * Script to module Simple EU Cookie Law
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

window.onload = function () {
        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000)); //

            d.setTime(d.getTime() + (exdays * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        var seucl_mod = document.getElementById("seucl_mod");
        if (getCookie("cookiesplugin_cookie") == "1") {
            seucl_mod.style.display = "none";
     
        } else {
            seucl_mod.style.display = "block";
     
        }

        var accept_cookie = document.getElementById("accept_cookie");
        
      accept_cookie.addEventListener("click", function () {
            setCookie("cookiesplugin_cookie", "1",seucl_mod.getAttribute("data-expires"));
          
          seucl_mod.style.display = "none";        
});
    };
       
