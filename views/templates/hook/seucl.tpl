{*
/**
 * Main module Simple EU Cookie Law
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */
 *}
 
<div id="seucl_mod" data-expires="{$expires|escape:'htmlall':'UTF-8'}" style="color:{$font_kolor|escape:'htmlall':'UTF-8'};background:{$kolor|escape:'htmlall':'UTF-8'};{$pozycjacookie|escape:'htmlall':'UTF-8'}:0px;">
    <div class="col-sm-2"> 
        {if version_compare($smarty.const._PS_VERSION_,'1.7','>=')}
            <img src=" {$urls.base_url|escape:'htmlall':'UTF-8'}/modules/seucl/{$img_cookie|escape:'htmlall':'UTF-8'}" alt="Cookies image"/>
        {else}
            {if $is_https}
                <img src=" {$base_dir_ssl|escape:'htmlall':'UTF-8'}}/modules/seucl/{$img_cookie|escape:'htmlall':'UTF-8'}}" alt="Cookies image"/>
            {else}
                <img src=" {$base_dir|escape:'htmlall':'UTF-8'}}/modules/seucl/{$img_cookie|escape:'htmlall':'UTF-8'}}" alt="Cookies image"/>
            {/if}
        {/if}

    </div> 
    <div class="col-sm-8">
        <div>
            {*ID LANG: {$language.id} *}
            {$seucl[$language.id]|escape:'htmlall':'UTF-8'}
            {*<a href="{$policy_url}" title="{$policy_name}">{$policy_name}</a>*}
        </div> 
    </div>  
    <div class="col-sm-2">
        <div>  
            <button class="btn" id="accept_cookie" style="background:{$btn_bg|escape:'htmlall':'UTF-8'};color:{$btn_font|escape:'htmlall':'UTF-8'};">{$btn_wartosc|escape:'htmlall':'UTF-8'}</button>
        </div>
    </div>       
</div> 